/**
 * @file
 * @ingroup group_amplitude_generator
 * @brief Header file for AmplitudeGenerator.C
 */
/***********************************************************

File Name :
        AmplitudeGenerator.h

Original Author:
        Patrick Small

Description:


Creation Date:
        05 May 1999

Modification History:
	23 Dec 2007 - leap second changes.

	05 Dec 2016 - 64-bit system compliance updates performed
Usage Notes:

**********************************************************/

#ifndef ampgen_H
#define ampgen_H

// Various include files
#include <map>
#include <stdint.h>
#include "gcda.h"
#include "dreader.h"
#include "ChannelADA.h"
#include "RTApplication.h"
#include "Channel.h"
#include "Origin.h"
#include "Event.h"
#include "EventArchiver.h"
#include "Magnitude.h"
#include "TimeStamp.h"
#include "TimeWindow.h"
#include "Duration.h"
#include "SeismoFuncs.h"
#include "DatabaseAmpGen.h"
#include "Tlay.h"


// Definition of a Channel-ADA Reader mapping
typedef std::map<Channel, ChannelADA,  std::less<Channel>, std::allocator<std::pair< const Channel, ChannelADA> > > ChanReaderMap;

// Definition of an Origin list
typedef std::multimap<TimeStamp, Origin, std::less<TimeStamp>,
        std::allocator<std::pair<const TimeStamp, Origin> > > OriginsList;

// Definition of a Magnitude list 
typedef std::multimap<TimeStamp, Magnitude, std::less<TimeStamp>,
        std::allocator<std::pair<const TimeStamp, Magnitude> > > MagvalueList;

// Definition of a Event list 
typedef std::multimap<TimeStamp, Event, std::less<TimeStamp>,
        std::allocator<std::pair<const TimeStamp, Event> > > EventList;


// Definition of the possible amplitude summary conditions
typedef enum {AMP_BB_NOT_AVAIL, AMP_BB_ANY_OFF, AMP_BB_ALL_ON_GAPS, AMP_BB_ALL_ON_FULL} ampBBSummary;


// Definition of the possible amplitude summary conditions
typedef enum {AMP_SM_NOT_AVAIL, AMP_SM_ALL_OFF, AMP_SM_ANY_ON_GAPS, AMP_SM_ANY_ON_FULL} ampSMSummary;


class AmplitudeGenerator : public RTApplication 
{
    
 private:
    char tridefsfile[MAXSTR];
    char adafile[MAXSTR];
    char ada_key_name[MAXSTR];
    char network[DB_MAX_AUTH_LEN+1];
    char source[DB_MAX_SUBSOURCE_LEN+1];
    char velocity_model_filename[MAXSTR];
    char archive_directory[MAXSTR];
    int proptime;
    int originupdate;
    char eventsubject[MAXSTR];
    char ampsubject[MAXSTR];
    double minmag;
    char amplogdir[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    int delay_secs;
    double distance_min;
    double distance_max;
    double snr_cutoff;
    int commit_amps;

    Tlay tlay;

    key_t ada_key;      // needed for amp reader creation now

    Generic_CDA<struct amplitude_data_type> *ada;
    EventArchiver old_events;
    ChanReaderMap chanreaders;
    MagvalueList maglist;
    OriginsList originlist;
    EventList evidlist;

    int _Cleanup();
    int _CreateADA(Channel &chan, ChannelReaderDB &db);
    int _LoadChannels();
    ampScale _GetScale(Channel chan, struct amplitude_data_type *sarray, 
		  int numsamp, int inst_type);
    int _HaveFullWin(Channel chan, int numsamp, Duration dur);
    int _AnyGappyADA(Channel , amplitude_data_type*, int);
    int _GetAmplitudes(Origin &org, double magnitude, int &numamps);
    int _FillAmp(Amplitude &a, Channel chan, ampType atype, 
		 ampScale scale, TimeStamp &timestamp,
		 struct amplitude_data_type &sample, TimeStamp winstart,
		 Duration dur, int fullwin);
    int _FindPeaks(TimeStamp winstart, Duration dur, Channel chan, 
		   TimeStamp *timestamps, 
		   struct amplitude_data_type *sarray, int numsamp,
		   AmpList &al, int inst_type);
    int _GetPeakAmps(TimeStamp origtime, Channel chan, ChannelADA ada,
		      AmpList &al);
    int _GetWindowedAmps(Origin &org, Channel chan, ChannelADA chanada, 
			AmpList &al, double magnitude);
    int _SendAmpSignal(uint32_t evid);
    int _GetBBSummary(ChanAmpList &cal, ampBBSummary &sum);
    int _GetSMSummary(ChanAmpList &cal, ampSMSummary &sum);
    int _CheckDigital(StationAmpList &hhal, StationAmpList &hlal,
		      ChanAmpList &chans, int &numamps);
    int _CheckAnalog(StationAmpList &anal, ChanAmpList &chans, int &numamps);
    int _StripDuplicates(ChanAmpList &chans, int &numamps, 
			 ChanAmpList &existing);
    int _DumpSummary(uint32_t evid, Origin &org);
    int _GetSearchWindow(Origin &org, const Channel &chan,
                            double mag, TimeWindow &win, double &dist);
    int _ProcessEvent(Origin &origin, double magnitude, uint32_t evid);
    void _ResetWaketime();
    void _removeArchivedEvent(uint32_t evid);

 public:
    
    AmplitudeGenerator();
    ~AmplitudeGenerator();

    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
    int DoEvent(uint32_t evid);
};


#endif
