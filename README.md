:no_entry_sign: **CAUTION, WORK IN PROGRESS** :no_entry_sign:
# Amplitude Generator (ampgen)	

Ampgen calculates peak amplitude values and stores them in a database. It does the following:

  * Selects the set of channels that should be scanned for peak amplitudes  
  * Determines the time window when peaks are likely to be found
  * Scans the ADA for peak values in that window (PGD, PGV, PGA, SP30, SP01, and SP03). The peaks were calculated and put into the ADA by RAD2  
  * Writes the peaks for each channel to the database  
  * Sends a CMS message when done



