/**
 * @file
 * @ingroup group_amplitude_generator 
 * @brief Header file for DatabaseAmpGen.C
 **/
/***********************************************************

File Name :
        DatabaseAmpGen.h

Original Author:
        Patrick Small

Description:


Creation Date:
        05 May 1999

Modification History:
	05 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef database_ampgen_H
#define database_ampgen_H

// Various include files
#include <list>
#include <map>
#include <stdint.h>
#include "Origin.h"
#include "Station.h"
#include "Channel.h"
#include "Amplitude.h"
#include "Database.h"


// Definition of an Amplitude list
typedef std::list<Amplitude, std::allocator<Amplitude> > AmpList;


// Definition of a Channel-Amplitude mapping
typedef std::map<Channel, AmpList, std::less<Channel>, std::allocator<std::pair< const Channel, AmpList> > > ChanAmpList;


// Definition of a Station-Amplitude mapping
typedef std::map<Station, ChanAmpList, std::less<Station>, std::allocator<std::pair< const Station, ChanAmpList> > > StationAmpList;

/// Class for database transactions for ampgen. Derived from aqms-libs/tndb/Database
class DatabaseAmpGen : public Database
{
 private:
    int _GetOriginID(uint32_t evid, uint32_t &orid);
    int _GetAmplitudes(uint32_t orid, ChanAmpList &cal);

 protected:

 public:
    DatabaseAmpGen();
    DatabaseAmpGen(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseAmpGen();

    int GetOrigin(uint32_t evid, double minmag, Origin &org, double &mag);
    int WriteAmplitudes(uint32_t evid, const char *network,
			const char *source, ChanAmpList &cal, Origin &org, int commit_amps);
    int UpdateOrigin(Origin &org);

};


#endif
