/**
 * @file
 * @ingroup group_amplitude_generator 
 * @brief Header for ChannelADA.C
 **/
/***********************************************************

File Name :
        ChannelADA.h

Original Author:
        Patrick Small

Description:


Creation Date:
        05 May 1999

Modification History:
	23 Dec 2007 - leap second changes.


Usage Notes:


**********************************************************/

#ifndef channel_ada_H
#define channel_ada_H


//Various include files
#include "dreader.h"
#include "ddcr.h"
#include "AmplitudeADA.h"


// Definition of the less-than comparator for Channel objects
typedef Defined_Data_Channel_Reader<struct amplitude_data_type> AmpReader;

/// Class for channel's ADA reader
class ChannelADA
{
 private:

 public:
    AmpReader *reader;

    ChannelADA();
    ChannelADA(const ChannelADA &ca);
    ~ChannelADA();

    ChannelADA& operator=(const ChannelADA &ca);
};

#endif
