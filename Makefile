########################################################################
#
# Makefile     : Amplitude Generator
#
# Author       : Patrick Small
#
# Last Revised : April 20, 1999
#
########################################################################


########################################################################
# Standard definitions and macros
#
#######################################################################

include $(DEVROOT)/shared/makefiles/Make.includes
BIN	= ampgen

# DEBUG = -DAMP_INSERT_DEBUG

INCL	= $(RTSTDINCL) $(EWINCL) -I$(DEVROOT)/lib/ewlib/include

LIBS	= $(RTSTDLIBS) -lew -lgcda -lpthread -lgfortran

BINOBJS= Main.o AmplitudeGenerator.o EventSignalHandler.o ChannelADA.o DatabaseAmpGen.o

SEQOBJS= TestMain.o TestSeq.o DatabaseTestSeq.o


all:$(BIN)

ampgen:$(BINOBJS)
	$(CC) $(CFLAGS) $(BINOBJS) -o $@ $(LIBS) $(EWOBJS)

testseq:$(SEQOBJS)
	$(CC) $(CFLAGS) $(SEQOBJS) -o $@ $(LIBS)

.C.o: 
	$(CC) $< -c $(CFLAGS) $(INCL)  $(DEBUG)

clean:
	-rm -f *.o *~ core $(BIN)
	-rm -rf SunWS_cache
