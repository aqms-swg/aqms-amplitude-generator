/**
 * @file
 * @ingroup group_amplitude_generator
 * @brief Header for TestSeq.h
 */
/***********************************************************

File Name :
        TestSeq.h

Original Author:
        Patrick Small

Description:


Creation Date:
        05 May 1999

Modification History:


Usage Notes:

**********************************************************/

#ifndef testseq_H
#define testseq_H

// Various include files
#include "RTApplication.h"
#include "DatabaseTestSeq.h"


class TestSeq : public RTApplication 
{
    
 private:

 public:
    
    TestSeq();
    ~TestSeq();

    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
};


#endif
