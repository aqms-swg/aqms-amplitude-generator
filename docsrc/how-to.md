How to compile and install the amplitude generator {#how-to-compile-install-ampgen}
==================================================
# Prerequisites

Ampgen depends on aqms-libs, aqms-rapid-amplitude-data, aqms-gcda(specifically the ada), aqms-cms (third party), aqms-alarming. Ensure that these are compiled and available.  

# Compile  

Run  

make -f Makefile  

This will generate a binary named *ampgen*.  

# Install  

Copy *ampgen* to desired location. Copy configuration file *ampgen.cfg*. Some institutions run two instances of ampgen. They use separate config files fo reach instance, one is configured to harvest amps from close-in stations, less than some configured distance from the hypocenter. The second is configured to handle more distant stations.  





