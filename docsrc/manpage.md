# ampgen {#man-ampgen}

Amplitude generator

## PAGE LAST UPDATED ON

2020-03-10

## NAME

ampgen

## VERSION and STATUS

v0.0.36 - 2018-09-18  
status: ACTIVE  

## PURPOSE

Calculates peak amplitude values and stores them in a database. 

  * Selects the set of channels that should be scanned for peak amplitudes  
  * Determines the time window when peaks are likely to be found
  * Scans the ADA for peak values in that window (PGD, PGV, PGA, SP30, SP01, and SP03). The peaks were calculated and put into the ADA by RAD2  
  * Writes the peaks for each channel to the database  
  * Sends a CMS message when done


## HOW TO RUN

```
usage: ./ampgen <config file>

Version: ./ampgen v0.0.36 - 2018-09-18

DB:Using Oracle SQL syntax
```

## CONFIGURATION FILE

The configuration file for ampgen is typically name ampgen.cfg and is described below. Format is

```
     <parameter name> <data type> <default value>
               <description>
```
**General configurations parameters**


**TSSOption** *string*  
CMS config file with path prefix.  

**Include** *string*  
External file to be included with path prefix.  

**Logfile** *string*  
Path with log file prefix where daily logs will be stored  

**LoggingLevel** *integer*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**ProgramName** *string*  
The unique name to allow this module to connect to CMS and be identifed.  

**HSInterval** *integer*  
???

**Application configuration parameters**
  
**Source** *string*  
Source ID of the amplitudes  

**Network** *string*  
Network ID of the operating network  

**EventSubject** *string* &nbsp;&nbsp; /signals/trimag/magnitude
The CMS signal to receive an event id (evid) on  

**AmpSubject** *string* &nbsp;&nbsp; /signals/ampgen/amplitude  
The CMS signal to publish the evid once amplitudes are written to the database.  Used by alarmdec.

**ArchiveDirectory** *string* &nbsp;&nbsp;  /app/aqms/run/tmp/events    
If present, then this directory is where the event id is persistently stored till the event is completely processed by ampgen. Once the amplitudes have been written to the database, then the evid is removed and processing has been completed.  

**ADAKeyName** *string* &nbsp;&nbsp; ADA_KEY  
The unique name to allow this module to connect to CMS and be identifed.  

**AmpLogDir** *string*  
Path where amgpen log files for each event it processes.  

**SNRcutoff** *float*  
Signal-to-Noise Ratio cutoff value  

**MinMag** *float*  
Amplitudes will only be calculated for events with magnitude >= MinMag  

**CommitAmps** *integer*
After every N inserts of amps, assocs, ampset rows, commit the data to the database  

**PropagationTime** *number*  
???

**VelocityModel** *string* &nbsp;&nbsp;   
Specifies the filename with path which contains the crustal model, by velocity layers.  VelocityModel overrides PropagationTime.

**DelaySeconds** *integer*  
???

**DistCutoffMin** *integer*  
???

**DistCutoffMax** *integer*  
???

**Database configuration parameters  **

*All parameters mentioned below can be put into an external file and included hear using **Include***

**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  

**DBPasswd** *string*  
The password for the database user account specified by DBUser.

**DBConnectionRetryInterval** *number*
Time to wait, in seconds, before re-attempting connection to the database.  

**DBMaxConnectionRetries** *number*  
Max number of times to attempt database connection.  

Note: Some institutions run two instances of ampgen. One is configured to harvest amps from close-in stations, less than some configured distance from the hypocenter. The second is configured to handle more distant stations.

## ENVIRONMENT


## DEPENDENCIES

  * Ampgen depends on the following aqms modules - *libs*, *rad2*, *ada*, *cms*, *alarming* 
  * Database tables *event*, *origin*, *netmag*, *amp*, *assocamp*, *assocamo*, *ampset* must be available
  * Postgres or Oracle client software

## MAINTENANCE

Make sure it keeps running 24/7 and database is available all the time.

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-amplitude-generator/-/issues

## MORE INFORMATION
 
“Continuous Monitoring of Ground-Motion Parameters”, BSSA, V89, pp. 311-316, Feb. 1999.
