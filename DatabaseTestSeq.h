/**
 * @file
 * @ingroup group_amplitude_generator
 * @brief Header for DatabaseTestSeq.C
 */
/***********************************************************

File Name :
        DatabaseTestSeq.h

Original Author:
        Patrick Small

Description:


Creation Date:
        05 May 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef database_testseq_H
#define database_testseq_H

// Various include files
#include "Database.h"
// include <stdint.h>

class DatabaseTestSeq : public Database
{
 private:

/*     int _GetIncrement(const char *seqname, int &incrvalue); */
/*     int _ParseRange(string range, uint32_t &lowval,  */
/* 		    uint32_t &highval); */
/*     int _ParseSequence(int incrvalue, string seq, SequenceList &sl); */
/*     int _GetNext(const char *seqname, int num, SequenceList &sl); */

 protected:

 public:
    DatabaseTestSeq();
    DatabaseTestSeq(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseTestSeq();

    int RunTest();
};


#endif
